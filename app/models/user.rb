require 'net/http'
require 'net/https'

class User < ActiveRecord::Base
serialize :image, Hash

  def self.getUserInfo(user_id)
    uri = URI.parse('https://slack.com/api/users.info')
    params = {token: 'xoxp-63729671648-63746414594-64146468359-ac6b2e4e50',
              user: user_id}
    uri.query = URI.encode_www_form(params)
    res = Net::HTTP.get_response(uri)
    info = JSON.parse(res.body) if res.is_a?(Net::HTTPSuccess)
    user_params = Hash.new
    user_params[:image] = Hash.new
    user_params[:name] = info['user']['real_name']
    user_params[:slack_id] = info['user']['id']
    user_params[:image][:size_24] = info['user']['profile']['image_24']
    user_params[:image][:size_32] = info['user']['profile']['image_32']
    user_params[:image][:size_48] = info['user']['profile']['image_48']
    user_params[:image][:size_72] = info['user']['profile']['image_72']
    user_params[:image][:size_192] = info['user']['profile']['image_192']
    user_params[:image][:size_512] = info['user']['profile']['image_512']
    puts(user_params)
    user_params 
  end
end
