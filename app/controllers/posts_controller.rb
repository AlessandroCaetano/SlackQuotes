class PostsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # POST /posts
  # POST /posts.json
  def create
    user_params = User.getUserInfo(params[:user])
    if(User.find_by_slack_id(params[:user]).nil?)
      @user = User.create(user_params)
    else
      @user = User.find_by_slack_id(params[:user]) 
    end
    #Remount post_params with data from user
    post_params = {}
    post_params[:date] = DateTime.now.to_s
    post_params[:user_id] = @user.id
    post_params[:text] = params[:text].sub!('/novaquote','')
    @post = Post.new(post_params)
    respond_to do |format|
      if @post.save
        format.json { render :show, status: :created, location: @post }
      else
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:text, :date)
    end
end
