/**
 * @param {object} data 
 */
var SlackBot = require('slackbots');
var request = require('request');
var bot = new SlackBot({
    token: 'xoxb-63737068935-BOwZi6jIKxgPWcxpYpIk98L5',
    name: 'Novabot'
});

bot.on('start',function() {
    var params = {
        icon_emoji: ':cat:'
    };
    bot.postMessageToChannel('general', 'Olá eu sou o novabot!');
});

bot.on('message', function(data){
    var res = String(data.text).split(" ");
    var bot_command = res[0]
    console.log(data)
    if(this._isChatMessage(data) &&
        this._isBotMessage(bot_command)){
        this._railsPostRequest(data)
    }
});

bot._isChatMessage = function(data){
    return data.type === 'message' && Boolean(data.text);
};

bot._isBotMessage = function(bot_command){
    return bot_command === '/novaquote'
};

bot._railsPostRequest = function(data){
    request({
        url: 'http://127.0.0.1:3000/posts.json',
        headers: {
        'Content-Type': 'application/json',
        },
        method: 'POST',
    json: {
        channel: data.channel,
        user: data.user,
        text: data.text,
        ts: data.timestamp,
        team: data.team
    }
    }, function(error, response, body){
        if(error) {
            console.log(error);
        } else {
        console.log(response.statusCode, body);
    }
    });
};
