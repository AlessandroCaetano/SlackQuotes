require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
  	User.stub(:getUserInfo).returns({image: 'test_image', name: 'test_name',
		slack_id: 'test_slack_id'})
  end

  test 'user class should be able to get info from slack api' do
    user_info = User.getUserInfo	
  	assert_instance_of(user_info,Hash)
  end

end
